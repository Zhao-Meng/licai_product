# licai_product

#### 介绍
金融理财产品获取（直接下载licaiproduct压缩包即可）

#### 软件架构
软件架构说明


#### 安装教程

1. python3.6
2. vscode
3. scrapy框架

#### 使用说明

1. 解压后运行run.py文件即可
2. 会提示先输入保存的文件名，按回车键结束
3. 再根据提示输入想要获取的银行数据
4. 再根据提示选择您要获取几页的数据

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 贡献者
 zhao meng


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)